from django.db import models

tipo_veiculo = ('carro', 'moto')


class Empresa(models.Model):
    nome = models.CharField(max_length=30, default="")
    url = models.CharField(max_length=30, default="")
    # código

    def __str__(self):
            return self.nome

class Veiculo(models.Model):

    # CARRO -> DB
    # Carro -> Visual

    TIPO_VEICULO = (
        ('CARRO', 'CARRO'),
        ('MOTO', 'MOTO'),
        ('CAMINHAO', 'CAMINHAO')
    )

    empresa = models.ForeignKey(Empresa, null=True)
    tipo = models.CharField(max_length=20, choices=TIPO_VEICULO, null=True)
    placa = models.CharField(max_length=30, default="", help_text='Digite o número da placa')
    fabricante = models.CharField(max_length=30, default="")    
    modelo = models.CharField(max_length=30, default="")
    placa = models.CharField(max_length=20, default="")
    cor = models.CharField(max_length=30, default="")

    cliente = models.CharField("Nome do cliente", max_length=30, default="")
    tel = models.CharField("Telefone de contato", max_length=30, default="")
    # descricao = models.TextField("Descrição", default=None, blank=True)

    def __str__(self):
            return "{} - {}".format(self.placa, self.tipo)
    
