from django.shortcuts import render
from django.http import HttpResponse
from simplemooc.core.models import Veiculo

# Create your views here.
def home(request):
    return render(request, 'home.html', {'name': 'Lucas'})

def veiculos(request):
    veiculos = Veiculo.objects.all()
    # filtro_empresa = Veiculo.objects.all().filter(empresa= "GogPS")

    return render(request, 'veiculos.html', {'veiculos': veiculos})