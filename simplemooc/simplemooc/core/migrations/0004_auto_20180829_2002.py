# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-08-29 20:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20180829_1958'),
    ]

    operations = [
        migrations.RenameField(
            model_name='empresa',
            old_name='first_name',
            new_name='nome',
        ),
        migrations.AddField(
            model_name='empresa',
            name='url',
            field=models.CharField(default='', max_length=30),
        ),
    ]
