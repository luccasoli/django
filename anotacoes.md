<h1>Estrutura MVT do Django</h1>

![GitHub LogoGitHub ](django_mvc_mvt_pattern.jpg)

<h1>Virtual Envriomnet:</h1>

* **Ativar:** `source <pasta>/bin/activate`
* **Desativar:** `deactivate`

<h1>Versões:</h1>
 
* **Django 1.11**
* **Python 3.5.2**
* **Instalar o Django:** `pip install Django==<número da versão>`

<h1>Comandos:</h1>

* **Ajuda:** `python3 manage.py help`
* **Criar Projeto:** `django-admin startproject <nome>`
* **Criar Aplicação:** `python manage.py startapp <nome>`
* **Iniciar Servidor Local:** `python manage.py runserver`
* **Inicializar o Data Base:** `python manage.py migrate`
* [DEPRECATED] **Armazenar alterações feitas no _model_ da aplicação** `python manage.py makemigrations <nome_do_app>`
    * (Deve-se primeiro adicionar a aplicação no settings.py INSTALLED_APPS) 

<h1>Detalhes da Estrutura</h1>

#### `__init__.py` -> Para o python reconhecer a pasta como pacote
    
<h1>Estrutura do Django</h1>
    
* `/mysite` -> Pasta raiz 
    *   `/myapp` -> Pasta da aplicação (existem N apps em um projeto) (pages)
    *   `/mysite` -> Pasta do projeto (app)

<h1>Migrações no Banco de Dados</h1>

* Mude seus modelos (em models.py).
* Rode `python manage.py makemigrations` para criar migrações para suas modificações
* Rode `python manage.py migrate` para aplicar suas modificações no banco de dados.