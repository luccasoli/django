from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return HttpResponse("Página inicial")

def detalhes(request, question_id):
    return HttpResponse("Você está procurando a questão {}".format(question_id))

def resultado(request, question_id):
    return HttpResponse("Você está procurando o resultado da questão {}.".format(question_id))

def voto(request, question_id):
    return HttpResponse('Você está votando na questão {}.'.format(question_id))